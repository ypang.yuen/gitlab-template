#!/bin/bash
function update_verion() {
  PACKAGEVER="${1}";
  PACKAGEFILE="${2}";
  TARGET_DIR="${3}"
  PACKAGEVER="${PACKAGEVER//v}"
  echo "-----";
  echo $PACKAGEVER;
  echo $PACKAGEFILE;
  echo $TARGET_DIR;
  echo "-----";
  find $TARGET_DIR -type f -name $PACKAGEFILE | xargs sed -i -e "/<PropertyGroup>/,/<\/PropertyGroup>/ s|<VersionPrefix>.*</VersionPrefix>|<VersionPrefix>$PACKAGEVER</VersionPrefix>|g"
}

function pack_version() {
  TARGET_DIR="${1}";
  SOURCE_CODE="${2}";
  CONFIG="${3}";
  OUTPUT="${4}";
  PROJECT_ID="${5:-$CI_PROJECT_ID}"
  ARG="$TARGET_DIR -c $CONFIG -o $OUTPUT"

  if [[ $CI_COMMIT_TAG == "" ]]; then
    ARG="$ARG --version-suffix $CI_COMMIT_SHORT_SHA";
  else
    update_verion "$CI_COMMIT_TAG" "*.csproj" "$TARGET_DIR";
  fi;
  
  echo $ARG | xargs dotnet pack

  dotnet nuget add source "${CI_API_V4_URL}/projects/${PROJECT_ID}/packages/nuget/index.json" --name gitlab --username gitlab-ci-token --password $CI_JOB_TOKEN --store-password-in-clear-text

  dotnet nuget push "./publish/*.nupkg" --source gitlab
}
