#!/bin/bash

function build_sam_param() {
  PREFIX="${1:-SAM_}"
  LIST=$(compgen -A variable | grep $PREFIX || true)
  ARG=""
  for i in ${LIST[@]}
  do
    V="ParameterKey=$i,ParameterValue=${!i}"
    ARG="$ARG$V "
  done
}